YAMAHA RTX シリーズ ルーター カスタム GUI

Anonymous PP 接続ユーザー編集
=============================

概要
----

YAMAHA RTX シリーズ ルーターで、
Anonymous PP の接続ユーザー (pp auth username) を編集するための
カスタム GUI です。

インストール
------------

anon-users ディレクトリ以下のファイル、ディレクトリを RTX シリーズ ルーターに保存します。
RTX シリーズ ルーターの httpd custom-gui user コマンドのファイル名には、
保存したファイル、ディレクトリの中の anon-user.html を指定します。

ライセンス
---------

Copyright 2018 Nakane, Ryuji

本リポジトリに含まれるファイルは以下のライセンスに従います。

- - -

> Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

> Apache License Version 2.0 (「本ライセンス」) に基づいてライセンスされます。
あなたがこのファイルを使用するためには、本ライセンスに従わなければなりません。
本ライセンスのコピーは下記の場所から入手できます。

    http://www.apache.org/licenses/LICENSE-2.0

> Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

> 適用される法律または書面での同意によって命じられない限り、
本ライセンスに基づいて頒布されるソフトウェアは、明示黙示を問わず、
いかなる保証も条件もなしに「現状のまま」頒布されます。
本ライセンスでの権利と制限を規定した文言については、
本ライセンスを参照してください。

- - -

ただし、本リポジトリ内の以下のライブラリ等はそれぞれのライセンスに従います。

- Fomt Awesome
    - https://fontawesome.com/license
- RequireJS
    - https://github.com/requirejs/requirejs
- es6-promise
    - https://www.npmjs.com/package/es6-promise
- fetch-ie8
    - https://www.npmjs.com/package/fetch-ie8
