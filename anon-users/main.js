/** Copyright 2018 Nakane, Ryuji
 **
 ** Licensed under the Apache License, Version 2.0 (the "License");
 ** you may not use this file except in compliance with the License.
 ** You may obtain a copy of the License at
 **      http://www.apache.org/licenses/LICENSE-2.0       
 ** Unless required by applicable law or agreed to in writing, software
 ** distributed under the License is distributed on an "AS IS" BASIS,
 ** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 ** See the License for the specific language governing permissions and
 ** limitations under the License.
 **/

require.config(
    { baseUrl: 'lib'
    , paths:
        { domReady: 'plugins/domReady'
        }
    }
);

//define(['require', 'domReady'], function (require, domReady) {
define(function (require) {
    var settings = {};
    var rtgui = require('rtgui');
    
    const logout = function () {
        rtgui.sendRTcommand('logout').then(
              function () { alert("ログアウトしました。ブラウザを終了してください。"); }
            , function () { alert("ログアウトできませんでした。"); }
        );
    };

    rtgui.sendRequest('settings.json').then(function (response) {
        response.json().then(function (json) {
            settings = utils.deepcopy(json);
        }).then(function () {
            const domReady = require('domReady');
            domReady(function() {
                document.getElementById('logout').onclick = logout;
                
                const anonymous_users = require('anon-users');
                anonymous_users(settings.anonymous_users || {});
            });
        });
    });

});