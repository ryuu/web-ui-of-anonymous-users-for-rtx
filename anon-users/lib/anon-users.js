/** Copyright 2018 Nakane, Ryuji
 **
 ** Licensed under the Apache License, Version 2.0 (the "License");
 ** you may not use this file except in compliance with the License.
 ** You may obtain a copy of the License at
 **      http://www.apache.org/licenses/LICENSE-2.0       
 ** Unless required by applicable law or agreed to in writing, software
 ** distributed under the License is distributed on an "AS IS" BASIS,
 ** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 ** See the License for the specific language governing permissions and
 ** limitations under the License.
 **/

require.config(
    { paths:
        { domReady: 'plugins/domReady'
        , promise: 'polyfill/es6-promise.auto.min'
        , fetch: 'polyfill/fetch'
        }
    }
);

define(function (require, exports, module) {
    require('promise');
    require('fetch');
    const utils = require('utils');
    const rtgui = require('rtgui');
    utils.loadCss('webfonts/fa-svg-with-js.css');
    utils.loadJs('webfonts/fontawesome-all.min.js');
    
    // 機能を切り替える連想配列
    var settings = {};

    // Anonymous User の属性を保存するオブジェクト
    AuthUserAttrs = function (init) {
        init = init || {};
        this._orig_password = this.password = init.password || '';
//        this._orig_myname   = this.myname   = init.myname   || '';
//        this._orig_mypass   = this.mypass   = init.mypass   || '';
        this._orig_ipv4addr = this.ipv4addr = init.ipv4addr || '';
        this._orig_ipv6addr = this.ipv6addr = init.ipv6addr || '';
        this._orig_ipv6mask = this.ipv6mask = init.ipv6mask || '';
        this._orig_ipv6prefix = this.ipv6prefix;
    };
    Object.defineProperty(AuthUserAttrs.prototype, 'ipv6prefix', {get: function () {
        return (this.ipv6addr && this.ipv6mask) ? this.ipv6addr + '/' + this.ipv6mask : '';
    }});
    Object.defineProperty(AuthUserAttrs.prototype, 'is_valid_ipv6prefix', {get: function () {
        return    ( this.ipv6addr &&  this.ipv6mask)
               || (!this.ipv6addr && !this.ipv6mask);
    }});
    Object.defineProperty(AuthUserAttrs.prototype, 'need_orginal_password', {get: function () {
        return this.is_edit && !this.password;
    }});
    Object.defineProperty(AuthUserAttrs.prototype, 'is_edit', {get: function () {
        return (  this._orig_password != this.password
               || this._orig_ipv4addr != this.ipv4addr
               || this._orig_ipv6addr != this.ipv6addr
               || this._orig_ipv6mask != this.ipv6mask
               ) && this.is_valid_ipv6prefix;
    }});

    // Anonymous User (AuthUserAttrs) を保存する連想配列
    var user_array = {};

    // ユーザーを表示するテーブル (tbody)
    var user_table = null;

    const main_anonymous_users = function (param_settings) {
        settings = param_settings || {};
        settings.use_ipv4 = utils.defaultget(settings, 'use_ipv4', false);
        settings.use_ipv6 = utils.defaultget(settings, 'use_ipv6', false);
        
        showAmonymousUsers();
    };

    const analyzeRemainUsenameLine = function (line, is_ignore_asterisk_password) {
        //【概要】  pp auth username コマンドの後半部分を解析する
        //【引数】  is_ignore_asterisk_password:
        //                  false ...   得たパスワードをそのまま保存する
        //                  true  ...   得たパスワードが一文字の '*' の時は、
        //                              パスワードを '' と仮定する
        is_ignore_asterisk_password = !!is_ignore_asterisk_password;
        var match = line.match(/^ *pp auth username (.*)$/);
        line = (match ? match[1] : line).trim() + ' ';
        
        var result = {};
        var username;
        [username, line] = rtgui.getMonolithicString(line, false);
        [result.password, line] = rtgui.getMonolithicString(line, is_ignore_asterisk_password);
        match = line.match(/^ *myname (.*)$/);
        if (match) {
            [result.myname, line] = rtgui.getMonolithicString(match[1]);
            [result.mypass, line] = rtgui.getMonolithicString(line, is_ignore_asterisk_password);
        }
        const re_ipv4 = /(?:0|1(?:[0-9]{0,2})?|2(?:[0-4][0-9]?|5[0-5]?|[6-9])?|[3-9][0-9]?)(?:\.(?:0|1(?:[0-9]{0,2})?|2(?:[0-4][0-9]?|5[0-5]?|[6-9])?|[3-9][0-9]?)){3}/ig
        const re_ipv6 = /((?:[0-9a-f]{1,4}:){7}(?:[0-9a-f]{1,4}|:)|(?:[0-9a-f]{1,4}:){6}:(?:[0-9a-f]{1,4})?|(?:[0-9a-f]{1,4}:){5}(?:(?::[0-9a-f]{1,4}){1,2}|:)|(?:[0-9a-f]{1,4}:){4}(?:(?::[0-9a-f]{1,4}){1,3}|:)|(?:[0-9a-f]{1,4}:){3}(?:(?::[0-9a-f]{1,4}){1,4}|:)|(?:[0-9a-f]{1,4}:){2}(?:(?::[0-9a-f]{1,4}){1,5}|:)|[0-9a-f]{1,4}:(?:(?::[0-9a-f]{1,4}){1,6}|:)|:(?:(?::[0-9a-f]{1,4}){1,7}|:))\/(1([01][0-9]?|2[0-7]?|[3-9])?|[2-9][0-9]?)/ig
        if ((match = re_ipv4.exec(line)) != null) {
            result.ipv4addr = match[0];
            line = line.slice(re_ipv4.lastIndex);
        }
        if ((match = re_ipv6.exec(line)) != null) {
            result.ipv6addr = match[1];
            result.ipv6mask = match[2];
            line = line.slice(re_ipv6.lastIndex);
        }
        return [username, new AuthUserAttrs(result)];
    };

    const getAmonymousUsers = function (is_ignore_asterisk_password) {
        //【概要】  show config pp anonymous を実行して、ユーザー情報を返す
        //【引数】  is_ignore_asterisk_password:
        //                  false ...   得たパスワードをそのまま保存する
        //                  true  ...   得たパスワードが一文字の '*' の時は、
        //                              パスワードを '' と仮定する
        //【戻り値】上記概要の処理を非同期で行う Promise インスタンス
        //          この Promise インスタンスは常に成功し、コールバック関数の
        //          引数にユーザー情報の連想配列をわたす
        //          なお、ルーターへのコマンド実行が失敗したときもこの Promise
        //          インスタンスは成功と看做すがコールバック関数には空の連想配
        //          列がわたされる
        //【解説】  escalation していないときは、パスワードが全て '*' になるた
        //          め、is_ignore_asterisk_password を true にして呼び出す
        is_ignore_asterisk_password = !!is_ignore_asterisk_password;
        return rtgui.sendRTcommand('show config pp anonymous').then(
            function(responseText) {
                const str_array = responseText.split('\r\n');
                const user_array = {};
                for (var i = 0; i < str_array.length; i++) {
                    var line = str_array[i].trim();
                    if (!line)  continue;
                    
                    if (line.search(/^pp auth username\b/) >= 0) {
                        var username, attrs;
                        [username,  attrs] = analyzeRemainUsenameLine(line, is_ignore_asterisk_password);
                        user_array[username] = attrs;
                    }
                }
                return Promise.resolve(user_array);
            },
            function (statusText) {
                return Promise.resolve({})
            }
        );
    };
    
    const makeUserPopup = function (call_node, username) {
        //【概要】  ユーザーの詳細を編集するポップアップを表示する
        //【引数】  call_node:  ポップアップ表示の元になる HTML ノード
        //          username:   編集対象のユーザー名、新規登録ユーザーのときは偽値
        //【戻り値】なし
        const user_attrs = new AuthUserAttrs(username in user_array ? user_array[username] : {});
        const popup = document.createElement('div');
        popup.classList.add('popup');
        popup.id = 'popup4' + username;
        popup.innerHTML =
                '<h3 id="title"></h3>'
              + '<table><tbody>'
              + '<tr><td>ユーザー名</td><td id="un_cell" /><td /></tr>'
              + '<tr><td>パスワード</td><td id="pw1_cell" /><td id="pw_notice" rowspan="2" /></tr>'
              + '<tr><td>パスワード (確認)</td><td id="pw2_cell" /></tr>'
              + (settings.use_ipv4 ? '<tr><td>IPv4 アドレス</td><td id="ipv4_cell" /><td /></tr>' : '')
              + (settings.use_ipv6 ? '<tr><td>IPv6 プレフィックス</td><td id="ipv6addr_cell" /><td id="ipv6mask_cell" /></tr>' : '')
              + '</tbody><tfoot>'
              + '<tr><td id="act_cell" colspan="2" style="text-align:right;" /></tr>'
              + '</tfoot></table>';
        if (username) {
            popup.querySelector('#title').innerHTML = 'ユーザー [' + username + '] を編集します';
            const notice = popup.querySelector('#pw_notice')
            notice.innerHTML = '※パスワードの変更が必要なときにだけ、入力してください。';
            notice.width = 200;
            notice.style.fontSize = 'smaller';
        }
        else {
            popup.querySelector('#title').innerHTML = 'ユーザーを追加します';
        }

        var input_username = username ? document.createTextNode(username) : utils.createInputElement('text');
        popup.querySelector('#un_cell').appendChild(input_username);
        
        const input_password1 = utils.createInputElement('password');
        popup.querySelector('#pw1_cell').appendChild(input_password1);
        const input_password2 = utils.createInputElement('password');
        popup.querySelector('#pw2_cell').appendChild(input_password2);
        
        const input_ipv4addr = settings.use_ipv4 ? utils.createInputElement('text') : null;
        if (settings.use_ipv4) {
            input_ipv4addr.value = user_attrs.ipv4addr;
            popup.querySelector('#ipv4_cell').appendChild(input_ipv4addr);
        }
        
        const input_ipv6addr = settings.use_ipv6 ? utils.createInputElement('text') : null;
        const input_ipv6mask = settings.use_ipv6 ? utils.createInputElement('text') : null;
        if (settings.use_ipv6) {
            input_ipv6addr.value = user_attrs.ipv6addr;
            input_ipv6mask.value = user_attrs.ipv6mask;
            input_ipv6mask.size = 5;
            popup.querySelector('#ipv6addr_cell').appendChild(input_ipv6addr);
            popup.querySelector('#ipv6mask_cell').appendChild(document.createTextNode('/'));
            popup.querySelector('#ipv6mask_cell').appendChild(input_ipv6mask);
        }
        
        const button_cancel = document.createElement('a');
        button_cancel.href = '#';
        button_cancel.id = 'cancel';
        button_cancel.classList.add('button');
        button_cancel.innerHTML =
              '<span class="fas fa-share"></span>'
            + '\u00a0取消';
        const button_regist = document.createElement('a');
        button_regist.href = '#';
        button_regist.id = 'regist';
        button_regist.classList.add('button', 'submit');
        button_regist.innerHTML =
              '<span class="far fa-check-circle"></span>'
            + '\u00a0登録';
        const act_cell = popup.querySelector('#act_cell')
        act_cell.appendChild(button_cancel);
        act_cell.appendChild(document.createTextNode('\u00a0'));
        act_cell.appendChild(button_regist);
        
        const overlay = document.createElement('div');
        overlay.classList.add('full-overlay');
        overlay.appendChild(popup);
        
        document.body.appendChild(overlay);
        const {left: target_left, top: target_top} = utils.getAbsolutePosition(call_node);
        popup.style.position = 'absolute';
        popup.style.left = target_left + call_node.offsetWidth;
        popup.style.top = target_top + (call_node.offsetHeight - popup.offsetHeight) / 2;
        popup.querySelector('input').focus();
        
        var elem_parent_input = Array.prototype.slice.call(
            document.body.querySelectorAll('a[href], input, button, [tabindex]')
        ).reduce(function (result, node) {
            if (!node.closest('#{id}'.F(popup)) && node.tabIndex >= 0){
                result.push([node, node.tabIndex]);
                node.tabIndex = -1;
            }
            return result;
        }, []);
        
        const popupClose = function (event) {
            if (event && event.target.classList.contains('disabled'))    return;
            for (var input of elem_parent_input)  input[0].tabIndex = input[1];
            overlay.parentNode.removeChild(overlay);
        };
        const displayErrorOnPopup = function (base_node, id, message) {
            const base_row = base_node.closest('tr');
            var error_row = base_row.closest('tbody,table').querySelector('#' + id);
            if (message) {
                if (!error_row) {
                    error_row = base_row.parentNode.insertRow(base_row.rowIndex + 1);
                    error_row.id = id;
                    error_row.classList.add('error');
                }
                while (error_row.cells.length)  error_row.deleteCell(-1);
                const error_cell = error_row.insertCell();
                error_cell.colSpan = 2;
                error_cell.innerHTML = message;
            }
            else {
                if (error_row)  error_row.parentNode.removeChild(error_row);
            }
        };
        const chkUsername = function (event) {
            if(input_username.nodeName === '#text')  return input_username.textContent;
            const username = input_username.value = input_username.value.trim();
            if (event && event.target.classList.contains('disabled'))    return username;
            
            var error_msg = '';
            if (username) {
                if (username.search(/[^a-zA-Z0-9_-]/) >= 0) {
                    error_msg = 'ユーザー名には、半角英数字とハイフン(-)、アンダーバー(_) 以外の文字は使えません。';
                }
                else if (username in user_array && !user_array[username]) {
                    error_msg = 'ユーザーは既に登録されています。';
                }
            }
            else if (event === undefined) {
                error_msg = 'ユーザー名を入力してください。';
            }
            displayErrorOnPopup(input_username, 'error_row_un', error_msg);
            return error_msg ? '' : username;
        };
        const chkPassword = function (event) {
            if (event && event.target.classList.contains('disabled'))    return true;
            const pw1 = input_password1.value = input_password1.value.trim();
            const pw2 = input_password2.value = input_password2.value.trim();
            var error_msg = '';
            if (pw1 && pw2 && pw1 != pw2) {
                error_msg = 'パスワードが間違っています。';
            }
            else if (input_username.nodeName !== '#text' && (!pw1 || !pw2)) {
                error_msg = 'パスワードを入力してください。';
            }
            else {
                user_attrs.password = pw1;
            }
            displayErrorOnPopup(input_password2, 'error_row_pw', error_msg);
            return !error_msg;
        };
        const chkIpv4addr = function (event) {
            if (event && event.target.classList.contains('disabled'))  return true;
            if (!settings.use_ipv4)  return true;
            const ipv4addr = input_ipv4addr.value = input_ipv4addr.value.trim();
            const re_ipv4 = /^(?:0|1(?:[0-9]{0,2})?|2(?:[0-4][0-9]?|5[0-5]?|[6-9])?|[3-9][0-9]?)(?:\.(?:0|1(?:[0-9]{0,2})?|2(?:[0-4][0-9]?|5[0-5]?|[6-9])?|[3-9][0-9]?)){3}$/i
            var error_msg = '';
            if (ipv4addr && !re_ipv4.test(ipv4addr)) {
                error_msg = '誤った IPv4 アドレスです。';
            }
            else {
                user_attrs.ipv4addr = ipv4addr;
            }
            displayErrorOnPopup(input_ipv4addr, 'error_row_v4addr', error_msg);
            return !error_msg;
        };
        const chkIpv6prefix = function (event) {
            if (event && event.target.classList.contains('disabled'))  return true;
            if (!settings.use_ipv6)  return true;
            const ipv6addr = input_ipv6addr.value = input_ipv6addr.value.trim();
            const ipv6mask = input_ipv6mask.value = input_ipv6mask.value.trim();
            var error_msg = '';
            const re_ipv6addr = /^(?:(?:[0-9a-f]{1,4}:){7}(?:[0-9a-f]{1,4}|:)|(?:[0-9a-f]{1,4}:){6}:(?:[0-9a-f]{1,4})?|(?:[0-9a-f]{1,4}:){5}(?:(?::[0-9a-f]{1,4}){1,2}|:)|(?:[0-9a-f]{1,4}:){4}(?:(?::[0-9a-f]{1,4}){1,3}|:)|(?:[0-9a-f]{1,4}:){3}(?:(?::[0-9a-f]{1,4}){1,4}|:)|(?:[0-9a-f]{1,4}:){2}(?:(?::[0-9a-f]{1,4}){1,5}|:)|[0-9a-f]{1,4}:(?:(?::[0-9a-f]{1,4}){1,6}|:)|:(?:(?::[0-9a-f]{1,4}){1,7}|:))$/i
            const re_ipv6mask = /^(?:1(?:[01][0-9]?|2[0-7]?|[3-9])?|[2-9][0-9]?)$/i
            if (ipv6addr && !re_ipv6addr.test(ipv6addr)) {
                error_msg = '誤った IPv6 アドレスです。';
            }
            else if (ipv6mask && !re_ipv6mask.test(ipv6mask)) {
                error_msg = '誤った IPv6 マスクです。';
            }
            else if ((ipv6addr && !ipv6mask) || (!ipv6addr && ipv6mask)) {
                error_msg = 'IPv6 のアドレスとマスクの片方を空にはできません。'
            }
            else {
                user_attrs.ipv6addr = ipv6addr;
                user_attrs.ipv6mask = ipv6mask;
            }
            displayErrorOnPopup(input_ipv6addr, 'error_row_v6addr', error_msg);
            return !error_msg;
        };
        const registUser = function (event) {
            if (event.target.classList.contains('disabled'))  return;
            const username = chkUsername();
            chkPassword(), chkIpv4addr(), chkIpv6prefix();
            if(popup.querySelector('tr.error'))  return;
            
            if(user_attrs.is_edit) {
                const user_row = username in user_array ? call_node.closest('tr') : insertUserRow(username, user_table);
                if (user_row) {
                    user_row.querySelector('#state_unedit').hidden = true;
                    user_row.querySelector('#state_edited').hidden = false;
                }
                user_array[username] = user_attrs;
                
                var button_submit = document.getElementById('sec_router_info').querySelector('#save .button.submit');
                button_submit.classList.remove('disabled');
                button_submit.tabIndex = 0;
            }
            popupClose();
        };
        
        if (input_username.nodeName.toLowerCase() == 'input')  input_username.onchange = chkUsername;
        input_password1.onchange = input_password2.onchange = chkPassword;
        if (input_ipv4addr)  input_ipv4addr.onchange = chkIpv4addr;
        if (input_ipv6addr)  input_ipv6addr.onchange = chkIpv6prefix;
        if (input_ipv6mask)  input_ipv6mask.onchange = chkIpv6prefix;
        button_regist.onclick = registUser;
        button_cancel.onclick = popupClose;
        return popup;
    };

    const editUser = function (event) {
        //【概要】  ユーザー表示テーブルの現在の行の属性を編集する
        //【引数】  なし
        //【戻り値】なし
        const target = event.target;
        if (target.classList.contains('disabled'))    return;
        makeUserPopup(target, target.closest('#user_row').querySelector('#name_cell').textContent);
    };

    const delUser = function (event) {
        //【概要】  ユーザー表示テーブルから現在の行を削除する
        //【引数】  なし
        //【戻り値】なし
        if (event && event.target.classList.contains('disabled'))    return;
        const user_line = event.target.closest('#user_row');
        const username = user_line.querySelector('#name_cell').textContent;
        user_line.parentNode.removeChild(user_line);
        if (username in user_array)  user_array[username] = null;
    };

    const insertUserRow = function (username, table, index) {
        //【概要】  テーブル ボディにユーザー行を一行追加する
        //【引数】  username:   ユーザー名
        //          table:      テーブル ボディ要素 (table 要素または tbody 要素)
        //          index:      行を追加する位置、負数のときは最下行に追加
        //【戻り値】追加したユーザー行の要素 (row 要素)
        const max = table.rows.length
        index = (index === undefined || index > max) ? max : index;
        
        const row = table.insertRow(index);
        row.id = 'user_row';
        
        const cell_username = row.insertCell();
        cell_username.id = 'name_cell';
        cell_username.appendChild(document.createTextNode(username));
        
        const button_edituser = row.insertCell().appendChild(document.createElement('a'));
        button_edituser.href = '#';
        button_edituser.id = 'edituser';
        button_edituser.classList.add('button');
        button_edituser.innerHTML =
              '<span id="state_unedit">'
            +   '<span class="fa-layers fa-fw">'
            +     '<span class="fa fa-edit"></span>'
            +   '</span>'
            + '</span>'
            + '<span id="state_edited">'
            +   '<span class="fa-layers fa-fw">'
            +     '<span class="fa fa-edit" data-fa-transform="shrink-2 right-1 down-1"></span>'
            +     '<span class="fas fa-circle" data-fa-transform="shrink-3 left-5 up-3"></span>'
            +     '<span class="fas fa-check" style="opacity:0.8;" data-fa-transform="shrink-4 left-5 up-3"></span>'
            +   '</span>'
            + '</span>'
            + '\u00a0編集';
        button_edituser.onclick = editUser;
        const state_unedit = button_edituser.querySelector('#state_unedit');
        state_unedit.hidden = false;
        const state_edited = button_edituser.querySelector('#state_edited');
        state_edited.querySelector('.fa-circle').style.color = getComputedStyle(button_edituser).backgroundColor;
        state_edited.hidden = true;
        
        const button_deluser = row.insertCell().appendChild(document.createElement('a'));
        button_deluser.href = '#';
        button_deluser.id = 'remove';
        button_deluser.classList.add('button');
        button_deluser.innerHTML =
              '<i class="fa fa-times-circle fa-fw"></i>'
            + '\u00a0削除';
        button_deluser.onclick = delUser;
        
        return row;
    };

    const addUser = function (event) {
        if (event && event.target.classList.contains('disabled'))    return;
        makeUserPopup(event.target, '');
    };

    const saveUser = function (event) {
        // :TODO: パスワードの変更無しに、ipv4,ipv6 が変更されているときは 昇格してパスワードを取得すること。
        if (event && event.target.classList.contains('disabled'))    return;
        const prepare = Promise.resolve({});
        if(Object.keys(user_array).some(function (username) {
            return user_array[username].is_edit && !user_array[username].password;
        })) {
            prepare = rtgui.escalation('anonymous').then(function () {return getAmonymousUsers(false);});
        }
        prepare.then(function (password_array) {
            var cmd_list = Object.keys(user_array).reduce(function (cmd_list, username) {
                if (user_array[username]) {
                    if (user_array[username].is_edit) {
                        var user_attrs = new AuthUserAttrs(user_array[username]);
                        if (!user_attrs.password)  user_attrs.password = password_array[username].password;
                        user_attrs.password = user_attrs.password.replace(/([\\"])/g, '\\$1');
                        if (/["' ]/.test(user_attrs.password))  user_attrs.password = '"' + user_attrs.password + '"';
                        var cmd = 'pp auth username ' + username + ' ' + user_attrs.password;
                        if (user_attrs.ipv4addr)    cmd += ' ' + user_attrs.ipv4addr;
                        if (user_attrs.ipv6prefix)  cmd += ' ' + user_attrs.ipv6prefix;
                        cmd_list.push(cmd);
                    }
                }
                else {
                    cmd_list.push('no pp auth username ' + username);
                }
                return cmd_list;
            },
            []);
            if (cmd_list.length) {
                cmd_list.unshift('pp select anonymous');
                rtgui.sendRTcommand(cmd_list, true).then(function (responseText) {
                    const msg_area = document.getElementById("command_result");
                    msg_area.innerHTML = 'コマンドを実行しました。<br /><hr />' + utils.convertText(responseText);
                    setTimeout(function (msg_area) { msg_area.innerHTML = ''; }, 5000, msg_area);
                    showAmonymousUsers();
                });
            }
        });
    };

    const showAmonymousUsers = function () {
        //【概要】  現在匿名接続のユーザー情報の編集画面を表示する
        //【引数】  なし
        //【戻り値】なし
        getAmonymousUsers(true).then(function (temp_user_array) {
            user_array = temp_user_array;
            
            const form_node = document.createElement('form');
            form_node.action = '#';
            form_node.innerHTML = '<h2>ユーザー</h2>';
            const table_node = form_node.appendChild(document.createElement('table'));
            table_node.createTHead().insertRow().innerHTML = '<th class="tcell">ユーザー名</th><th colspan="2" />';
            
            user_table = table_node.createTBody();
            user_table.id = 'user_table';
            Object.keys(user_array).sort().forEach(function (username, idx) {
                insertUserRow(username, user_table, idx);
            });
            
            const tfoot = table_node.createTFoot();
            tfoot.innerHTML =
                  '<tr><td colspan="3"><table  style="width:100%;">'
                + '<tr><td id="adduser" style="width:100%;" /><td id="redo" nowrap style="text-align:center;" /><tr>'
                + '<tr><td /><td id="save" nowrap style="text-align:center;" /></tr>'
                + '</td></tr></table>';
            
            const button_adduser = tfoot.querySelector('#adduser').appendChild(document.createElement('a'));
            button_adduser.href = '#';
            button_adduser.style.float = 'left';
            button_adduser.classList.add('button');
            button_adduser.innerHTML =
                  '<span class="fas fa-user-plus"></span>'
                + '\u00a0ユーザー追加';
            button_adduser.onclick = addUser;
            
            const button_redo = tfoot.querySelector('#redo').appendChild(document.createElement('a'));
            button_redo.href = '#';
            button_redo.classList.add('button');
            button_redo.innerHTML =
                  '<span class="fas fa-redo"></span>'
                + '\u00a0元に戻す (再取得)';
            button_redo.onclick = showAmonymousUsers;
            
            const button_save = tfoot.querySelector('#save').appendChild(document.createElement('a'));
            button_save.href = '#';
            button_save.classList.add('button', 'submit', 'disabled');
            button_save.tabIndex = -1;
            button_save.innerHTML =
                  '<span class="fa-layers fa-fw">'
                +   '<span class="fas fa-cog"></span>'
                +   '<span class="fas fa-circle" data-fa-transform="shrink-7 left-4 down-3"></span>'
                +   '<span class="fas fa-angle-double-right" data-fa-transform="shrink-5 left-5 down-3"></span>'
                + '</span>'
                + '\u00a0保存';
            button_save.querySelector('.fa-circle').style.color = getComputedStyle(button_save).backgroundColor;
            button_save.onclick = saveUser;
            
            const insert_node = document.getElementById('sec_router_info');
            while (insert_node.hasChildNodes())  insert_node.removeChild(insert_node.firstChild);
            insert_node.appendChild(form_node);
        });
    };
    
    return main_anonymous_users;
});
