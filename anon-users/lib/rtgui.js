/** Copyright 2018 Nakane, Ryuji
 **
 ** Licensed under the Apache License, Version 2.0 (the 'License');
 ** you may not use this file except in compliance with the License.
 ** You may obtain a copy of the License at
 **      http://www.apache.org/licenses/LICENSE-2.0       
 ** Unless required by applicable law or agreed to in writing, software
 ** distributed under the License is distributed on an 'AS IS' BASIS,
 ** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 ** See the License for the specific language governing permissions and
 ** limitations under the License.
 **/

require.config(
    { paths:
        { promise: 'polyfill/es6-promise.auto.min'
        , fetch: 'polyfill/fetch'
        }
    }
);

define(function (require, exports, module) {
    require('/custom/custom_gui_lib.js');
    require('promise');
    require('fetch');
    utils = require('utils');
    
    const sendRequest = exports.sendRequest = function (input, init) {
        //【概要】  fetch リクエストを送信する
        //【引数】  input   : 送信先 URL (fetch の input 引数)
        //          init    : リクエスト オプション (fetch の init 引数)
        //【戻り値】Promise オブジェクト
        //          成功時のコールバック関数の引数は、リクエスト送信後の Rsponse オブジェクト
        //          失敗時のコールバック関数の引数は、ステータス文字列を含むエラー オブジェクト
        init = init || {};
        init.credentials = init.credentials || 'same-origin';
        return fetch(input, init).then(function (response) {
            if (response.status == 200) {
                const msg_area = document.getElementById("http_state");
                if (msg_area)  msg_area.innerHTML = '';
                return Promise.resolve(response);
            }
            else {
                const msg = (response.status ? ('[' + response.status + '] ') : '') + response.statusText;
                return Promise.reject(new Error(msg));
            }
        }).catch(function(e) {
            if(!e.message && !e.response)  e.message = 'request timeout';
            const msg_area = document.getElementById("http_state");
            if (msg_area) msg_area.innerHTML = '通信エラーです。<br />' + e.message;
            return Promise.reject(e);
        });
    };

    const sendRTcommand = exports.sendRTcommand = function (command) {
        //【概要】  RT シリーズ ルーターにコマンドを送信する
        //【引数】  command:    送信するコマンド文字列
        //                      二行以上のコマンドを送信するときは、各行を要素にした Array
        //                      コマンドに改行は含まない
        //          async:      true  - 非同期モード
        //                      false - 同期モード
        //【戻り値】Promise オブジェクト
        //          成功時のコールバック関数の引数は、リクエスト送信後の Rsponse.text オブジェクト
        //          失敗時のコールバック関数の引数は、ステータス文字列を含むエラー オブジェクト
        if (!command)   return;
        
        const cmd_array = ['#' + getSessionId(), '#echoback=on'];
        if (utils.tyepOf(command) == 'string')  cmd_array.push(command);
        else  Array.prototype.push.apply(cmd_array, command);
        const body = cmd_array.join('\r\n') + '\r\n';
        return sendRequest('/custom/execute', {method: 'POST', body: body})
               .then(function (response) {
                    return Promise.resolve(response.text());
               })
    };

    const escalation = exports.escalation = (function (num) {
        //【概要】  administrator に昇格するために pp enable/disable を実行する
        //【引数】  pp enable/disable の対象になる pp 番号、または 'anonymous'
        //【戻り値】常に成功する Promise オブジェクト
        //          コールバック関数の引数は、不定
        var is_escalated = false;
        
        num = parseInt(num || '', 10);
        num = !isNaN(num) && num > 0 ? num : 'anonymous';
        
        return function () {
            if (is_escalated)   return Promise.resolve();
            
            sendRTcommand('show config pp ' + num)
            .then(function(responseText) {
                const able_line = responseText.split('\r\n').find(function (line) {
                    return /\bpp\s+(en|dis)able\b/.test(line);
                });
                if (able_line === undefined) {
                    document.getElementById("command_result").innerHTML =
                        'コマンドを実行できませんでした。<br /><hr />' + convertText(responseText);
                    return Promise.reject();
                }
                return sendRTcommand('pp {0} {1}\r\n'.F(/\b(en|dis)able\b/.exec(able_line)[0], num));
            })
            .then(function(responseText) {
                is_escalated = true;
                return Promise.resolve();
            });
        };
    })();

    const getMonolithicString = exports.getMonolithicString = function (line, is_ignore_asterisk) {
        //【概要】  ひとつながりの文字列返す
        //【引数】  line:   元の文字列
        //          is_ignore_asterisk:
        //                  false ...   得た文字列をそのまま返す
        //                  true  ...   得た文字列が一文字の '*' の時は '' を
        //                              返す
        //【戻り値】ひとつながりの文字列と、引数 line のそれ以降の部分の配列
        //【解説】  与えられた文字列の先頭から空白文字を除き、そこから次の空
        //          白文字までの文字列を一繋がりの文字列とする
        //          ただし、\(円・バックスラッシュ), ' (一重引用符), " (二重
        //          引用符) を特殊文字として扱い、特殊文字を排除した文字列を
        //          取得する
        //          特殊文字の働きは、\ はその次の文字の扱いを一般文字に強制
        //          し、' または " は次の特殊文字としての '、" までの間の \
        //          を除く特殊文字の扱いを一般文字に強制する
        //          この働きは YAMAHA RT シリーズルーターの文字列解釈に基づく
        //              FAQ for YAMAHA RT Series / Config
        //                http://www.rtpro.yamaha.co.jp/RT/FAQ/Config/charactor-need-escape.html
        is_ignore_asterisk = !!is_ignore_asterisk;
        line = line.replace(/^\s+/, '');
        var idx = 0;
        var chr = line.charAt(idx);
        if (is_ignore_asterisk && chr == '*' && line.charAt(idx + 1) == ' ') return ['', line.slice(idx + 1)];
        
        var monolithic = '';
        var quote = '';
        for ( ; chr && (quote || chr != ' '); chr = line.charAt(++idx)) {
            if (quote == chr) {
                quote = '';
            }
            else if (!quote && (chr == '"' || chr == "'")){
                quote = chr;
            }
            else {
                if (chr == '\\' && line.charAt(idx + 1))    chr = line.charAt(++idx);
                monolithic += chr;
            }
        }
        return [monolithic, line.slice(idx)];
    };
});
