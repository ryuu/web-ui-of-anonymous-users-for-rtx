/** Copyright 2018 Nakane, Ryuji
 **
 ** Licensed under the Apache License, Version 2.0 (the 'License');
 ** you may not use this file except in compliance with the License.
 ** You may obtain a copy of the License at
 **      http://www.apache.org/licenses/LICENSE-2.0       
 ** Unless required by applicable law or agreed to in writing, software
 ** distributed under the License is distributed on an 'AS IS' BASIS,
 ** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 ** See the License for the specific language governing permissions and
 ** limitations under the License.
 **/

/////////////////////////////////
// Polyfill: Element.matches() //
/////////////////////////////////
if (!Element.prototype.matches)
    Element.prototype.matches = Element.prototype.msMatchesSelector;

/////////////////////////////////
// Polyfill: Element.closest() //
/////////////////////////////////
if (!Element.prototype.closest)
    Element.prototype.closest = function(s) {
        var el = this;
        if (!document.documentElement.contains(el)) return null;
        do {
            if (el.matches(s)) return el;
            el = el.parentElement || el.parentNode;
        } while (el !== null); 
        return null;
    };

/////////////////////////////////
// 文字列に format 関数を追加
/////////////////////////////////
if (String.prototype.format == undefined) {
    String.prototype.format = function(arg) {
        var rep_fn = undefined;
        if (typeof arg == "object") { // arg が Object のときは {prop1}、{prop2} のような Object のプロパティ名指定
            rep_fn = function(re, key) { return arg[key]; }
        }
        else { // {0}、{1} のような数値指定で、不定長引数
            const args = arguments; // ここで代入することで、内側の関数がこの時点の arguments を参照できる
            rep_fn = function(re, key) { return args[parseInt(key)]; }
        }
        return this.replace( /\{(\w*)\}/g, rep_fn );
    };
}
// string.F() を format 関数のショート ネームにする
if (String.prototype.F == undefined) {
    String.prototype.F = function () {
        return String.prototype.format.apply(this, Array.prototype.slice.call(arguments));
    }
}

define(function (require, exports, module) {
    // CSS の動的読み込み
    exports.loadCss = function (url) {
        const link = document.createElement('link');
        link.type = 'text/css';
        link.rel = 'stylesheet';
        link.href = url;
        document.getElementsByTagName('head')[0].appendChild(link);
    };
    
    // Javascript の動的読み込み
    exports.loadJs = function (url) {
        const script = document.createElement('script');
        script.src = url;
        document.getElementsByTagName('head')[0].appendChild(script);
    };
    

    //  与えられたオブジェクトの型を示す文字列 (小文字) を返す
    exports.tyepOf = function (obj) {
        // JavaScriptの型などの判定いろいろ - Qiita
        // https://qiita.com/amamamaou/items/ef0b797156b324bb4ef3
        var undefined;
        if (obj === undefined)  return 'undefined';
        if (obj === null)  return 'null';
        result = Object.prototype.toString.call(obj).slice(8,-1).toLowerCase();
        if (result === 'number' && isNaN(obj))  result = 'nan';
        return result
    };

    // オブジェクトのプロパティを返す。ただし、プロパティがなければ規定値を返す
    exports.defaultget = function (obj, prop, value) {
        var undefined;
        return obj[prop] === undefined ? value : obj[prop];
    };
    
    // オブジェクトをディープ コピーする
    var deepcopy;
    exports.deepcopy = deepcopy = function (object) {
        //      ES6のObject.assignがシャローコピーなのでディープコピーする方法を考える | Black Everyday Company
        //      https://kuroeveryday.blogspot.jp/2017/05/deep-clone-object-in-javascript.html
        var undefined;
        var rtn;
        if      (object === undefined)  rtn = undefined;
        else if (object === null)       rtn = null;
        else if (Array.isArray(object)) {
            rtn = object.slice(0) || [];
            rtn.forEach(function (n) {
                if (typeof n === 'object' && n !== {} || Array.isArray(n)) {
                    n = deepcopy(n);
                }
            });
        }
        else if (typeof object === 'object') {
            rtn = Object.assign({}, object);
            Object.keys(rtn).forEach(function (key) {
                if (typeof rtn[key] === 'object' && rtn[key] !== {}) {
                    rtn[key] = deepcopy(rtn[key]);
                }
            });
        }
        else {
            rtn = object;
        }
        return rtn;
    }

    // input エレメントを作成する
    exports.createInputElement = function (type) {
        //【引数】  type: input エレメントのタイプ
        //【戻り値】input エレメント
        const el = document.createElement('input');
        el.type = type || 'text';
        el.size = 20;
        return el;
    };

    // DOM ノードの表示絶対座標を返す
    exports.getAbsolutePosition = function (node) {
        //【引数】  node: 対象ノード
        //【戻り値】Web ページ (body 要素) 左上からの (x,y) 座標
        const {left, top} = node.getBoundingClientRect();
        const {left: bleft, top: btop} = document.documentElement.getBoundingClientRect();
        return { left: left - bleft, top: top - btop};
    };

    // 文字列の中にある特殊文字をHTML形式で表示できるように変換する
    exports.convertText = function (str) {
         //【引数】  str: 文字列
         //【戻り値】変換した文字列
        str = str.toString();
        str = str.replace(/&/g, "&amp;");
        str = str.replace(/</g, "&lt;");
        str = str.replace(/>/g, "&gt;");
        str = str.replace(/"/g, "&quot;");
        str = str.replace(/'/g, "&#39;");
        str = str.replace(/ /g, "&nbsp;");
        str = str.replace(/\t/g, "&nbsp;&nbsp;&nbsp;&nbsp;");
        str = str.replace(/\r?\n/g, "<br/>\n");
        return str;
    };
});
